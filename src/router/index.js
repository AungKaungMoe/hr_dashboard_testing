import { createRouter, createWebHistory } from 'vue-router'
import Dashboard from '../components/DashboardPage.vue'
import DashboardHome from '../views/HomePage.vue'
import Analytics from '../views/AnalyticsPage.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    // {
    //   path: '/',
    //   name: 'home',
    //   component: HomePage
    // },
    { path: '/', redirect: { name: 'DashboardHome' } },
    {
      path: '/dashboard',
      component: Dashboard,
      children: [
        { path: '/', redirect: { name: 'DashboardHome' } },
        { path: 'home', name: 'DashboardHome', component: DashboardHome },
        { path: 'analytics', name: 'analytics', component: Analytics}
      ]
    },
    { path: '/analytics', redirect: 'analytics' }
  ]
})

export default router
