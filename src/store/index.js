import { reactive } from "vue"
const state = reactive({
    sideBarOpen: false
})

const methods = {
    toggleSideBar() {
        return state.sideBarOpen = !state.sideBarOpen;
    }
}
export default {
    state,
    methods
}